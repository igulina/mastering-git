So far, I am really enjoying this course. It is a welcome change from the other courses at university with more
time and space for various questions. I really like that even when some people are shy to ask you something
(or admit they don't know), you repeat it again, even if it is something from previous lectures.

